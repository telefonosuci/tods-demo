import axios from 'axios'
import _assign from 'lodash/assign'

const BASE_CONF = {
    timeout: 60 * 1000
}

const BASE_URL = "http://localhost:3000"

const BASE_API_URL = `${BASE_URL}/rest/v2/tods-it`

const axiosInstance = axios.create(
    _assign(BASE_CONF, {
        baseURL: BASE_API_URL,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        },
    }),
)

export const GET = (path, config = {}) => {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get(path, config)
        .then(response => resolve(response))
        .catch(error => reject(error));
    });
};
