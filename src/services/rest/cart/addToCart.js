import { POST } from '@/services/rest';

const resource = "/cart/add"

export default async ({ product, successCB, failCB, config }) => {
    try {
        const { data } = await POST(`${resource}`, product, config);
        return (typeof successCB === 'function' && successCB(data)) || data;
    } catch (e) {
        return (typeof failCB === 'function' && failCB(e)) || e;
    }
};