import { GET } from '../index';
const resource = "/products"

const getProduct = async ({ productId, successCB, failCB, config }) => {

    

    try {
        const { data } = await GET(`${resource}/${productId}`, config)
        return (typeof successCB === 'function' && successCB(data)) || data
    } catch (e) {
        return (typeof failCB === 'function' && failCB(e)) || e
    }
}
export default getProduct