import { GET } from '@/services/rest';

const resource = "/products"

export default async ({ successCB, failCB, config }) => {
    try {
        const { data } = await GET(`${resource}`, config);
        return (typeof successCB === 'function' && successCB(data)) || data;
    } catch (e) {
        return (typeof failCB === 'function' && failCB(e)) || e;
    }
};