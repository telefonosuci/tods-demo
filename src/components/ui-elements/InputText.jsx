import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

const InputText = ({type, name, initialValue, placeholder, handler, required}) => {

    const inputChangedHandler = (event) => {
        setTouched(true)
        setValue(event.target.value)
        handler(name, event.target.value)
    }
    const [touched, setTouched] = useState(false) 
    const [error, setError] = useState(false) 
    const [errorMessage, setErrorMessage] = useState('') 
    const [value, setValue] = useState(initialValue) 

    useEffect(() => {
        
        if(!value && required ){
            setError(true)
            setErrorMessage('Il campo è obbligatorio')
        } else {
            setError(false)
        }
        
    }, [value, required])

    const debounceHandler = _.debounce((event) => inputChangedHandler(event), 500)

    return (
        <div className="tods-c-input-wrapper">
            <input 
                type={type} 
                name={name} 
                defaultValue={initialValue}
                className="tods-input-text" 
                placeholder={placeholder}
                onChange={(event) => debounceHandler(event)} />
            <label className="">{placeholder}{(required && '*')}</label>
            {error && touched && <span className="tods-c-input-wrapper__error-msg">{errorMessage}</span>}
        </div>
    )
}

InputText.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    initialValue: PropTypes.string,
    placeholder: PropTypes.string,
    handler: PropTypes.func,
    required: PropTypes.bool,
}

InputText.defaultProps = {
    type: 'text',
    name: '',
    initialValue: '',
    placeholder: '',
    handler: () => {},
    required: false,
}

export default InputText