import React from 'react'
import PropTypes from 'prop-types';
import noImage from '../../../assets/images/no-image.png';

const SlideImage = ({url, size, onClick, selected, main}) => {

    const addDefaultSrc = (ev) => {
        ev.target.src = noImage
    }

    return (
        <a href='#0' onClick={onClick} className={`${!main ? 'tods-c-slider__control-image-box': ''} ${selected ? 'tods-c-slider__control-image-box--selected' : ''}`}>
            <img alt='Slide' width={size} src={url} onError={addDefaultSrc} />
        </a>
    )
}

SlideImage.propTypes = {
    url: PropTypes.string,
    size: PropTypes.string,
    onClick: PropTypes.func,
    selected: PropTypes.bool,
    main: PropTypes.bool,
};

SlideImage.defaultProps = {
    url: 'text',
    size: '',
    onClick: () => {},
    selected: false,
    main: false,
};

export default SlideImage