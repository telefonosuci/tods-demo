import React, { useState } from 'react'
import PropTypes from 'prop-types';
import SlideImage from './SlideImage'

const SlideShow = ({images}) => {

    const [imgsState] = useState(images)
    const [indexState, setIndexState] = useState(0)
    const [, setSelectedImgState] = useState(images[0])

    
    const selectSlideHandler = index => () => {
        setIndexState(index)
        setSelectedImgState(imgsState[index])
    }

    const slideBackward = () => {
        if(imgsState[indexState -1]) {
            setIndexState(indexState -1)
        } else {
            setIndexState(imgsState.length - 1)
        }
    }

    const slideForward = () => {
        if(imgsState[indexState +1]) {
            setIndexState(indexState +1)
        } else {
            setIndexState(0)
        }
    }

    if(imgsState)
    return (
        
        <div className='tods-c-slider'>
        
            <div className='tods-c-slider__active'>
                <div className='tods-c-slider__active-controls tods-c-slider__active-controls--left' onClick={slideBackward}></div>
                <div className='tods-c-slider__absolute-container'>
                    <SlideImage size='100%' url={imgsState[indexState].url} main={true} />
                </div>
                <div className='tods-c-slider__active-controls tods-c-slider__active-controls--right' onClick={slideForward}></div>
            </div>
            
            <div className='tods-c-slider__controls'>
                
                {images && images.map((image, index) => 
                    <SlideImage selected={index === indexState} key={index} size='124px' url={image.url} onClick={selectSlideHandler(index)} /> 
                )}
            </div>
            
        </div>
    
    )
}

SlideShow.propTypes = {
    images: PropTypes.array
};

SlideShow.defaultProps = {
    images: []
};

export default SlideShow