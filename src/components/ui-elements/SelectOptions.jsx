import React, { useState } from 'react'
import PropTypes from 'prop-types';

const SelectOptions = ({options, name, label, initialValue, placeholder, handler}) => {
    const [, setValue] = useState(initialValue);

    const inputChangedHandler = (event) => {
        setValue(event.target.value);
        handler(event.target.value)
    }

    return (
        <div className="tods-c-option-select__wrapper">
            <label htmlFor={name}>{label}</label>
            <select 
                name={name} 
                className="tods-c-option-select" 
                value={initialValue} 
                placeholder={placeholder}
                onChange={(event) => inputChangedHandler(event)}>
                <option value=''>{placeholder}</option>
                {options && Object.keys(options).map((option, index) => 
                    <option key={index} value={option}>{option}</option>
                )}
            </select>
        </div>
    )
}

SelectOptions.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    initialValue: PropTypes.string,
    placeholder: PropTypes.string,
    handler: PropTypes.func,
};

SelectOptions.defaultProps = {
    type: 'text',
    name: '',
    label: '',
    initialValue: '',
    placeholder: '',
    handler: () => {}
};

export default SelectOptions