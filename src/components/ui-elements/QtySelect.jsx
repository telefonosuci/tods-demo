import React from 'react'
import PropTypes from 'prop-types';

const QtySelect = ({max, name, initialValue, placeholder, handler, label}) => {

    const inputChangedHandler = (event) => {
        handler(event.target.value)
    }

    const items = []

    for (let i = 1; i <= max; i++) {
        items.push(<option key={i} value={i}>{i}</option>)
    }

    return (
        <div className="tods-c-option-select__wrapper">
            <label htmlFor={name}>{label}</label>
            <select 
                name={name} 
                className="tods-c-option-select" 
                defaultValue={initialValue} 
                placeholder={placeholder}
                onChange={(event) => inputChangedHandler(event)} >
                <option value=''>{placeholder}</option> 
                    {items}
            </select>
        </div>
    )
}

QtySelect.propTypes = {
    max: PropTypes.number,
    name: PropTypes.string,
    initialValue: PropTypes.string,
    placeholder: PropTypes.string,
    handler: PropTypes.func,
    label: PropTypes.string,
};

QtySelect.defaultProps = {
    max: 10,
    name: '',
    initialValue: '',
    placeholder: '',
    handler: () => {},
    label: '',
};

export default QtySelect