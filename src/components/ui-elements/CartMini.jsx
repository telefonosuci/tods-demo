import React from 'react'
import PropTypes from 'prop-types';
import { useSelector} from 'react-redux'

const CartMini = () => {

    const count = useSelector(
        state => state.cartEntries.reduce((n, {qty}) => n + Number(qty), 0)
    )

    return (
        <div className='tods-c-cartmini'>
            cart ({count})
        </div>
    )
}

CartMini.propTypes = {
    type: PropTypes.string
};

CartMini.defaultProps = {
    qty: ''
};

export default CartMini