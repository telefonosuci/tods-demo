import React, { useState } from 'react'
import PropTypes from 'prop-types';

const RadioButton = ({options, name, initialValue, handler, description}) => {
    const [, setValue] = useState(initialValue);

    const inputChangedHandler = (event) => {
        setValue(event.target.value);
        handler(name, event.target.value)
    }

    return (
        <div className="tods-c-input-wrapper tods-c-input-wrapper--radio">
            {options && options.map((option, index) => 
                <div key={index} className="tods-c-input-wrapper__radio-el">
                    <span><input type='radio' name={name} value={option.value} onChange={inputChangedHandler} checked={initialValue ===  option.value} />{option.placeholder}</span>
                    <span className="tods-c-input-wrapper__radio-desc">{option.description}</span>
                </div>
            )}
        </div>
    )
}

RadioButton.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    initialValue: PropTypes.string,
    placeholder: PropTypes.string,
    handler: PropTypes.func,
};

RadioButton.defaultProps = {
    type: 'text',
    name: '',
    initialValue: '',
    placeholder: '',
    handler: () => {}
};

export default RadioButton