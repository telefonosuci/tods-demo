import React from 'react'
import PropTypes from 'prop-types';

const Button = ({type, name, text, icon, onClick, disabled}) => {

    return (
        <button className={`tods-c-button ${icon ? 'tods-c-button--icon' : ''}`} onClick={onClick} disabled={disabled}>{icon && <i className={`im im-${icon}`}></i>}{text}</button>
    )
}

Button.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    text: PropTypes.string,
    icon: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
};

Button.defaultProps = {
    type: 'button',
    name: '',
    text: '',
    icon: '',
    onClick: () => {},
    disabled: false,
};

export default Button