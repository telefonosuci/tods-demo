import React from 'react'
import PropTypes from 'prop-types';

const Footer = ({text}) => {
    
    return (
        <div className='tods-c-footer'>
           {text}
        </div>
    )
}

Footer.propTypes = {
    text: PropTypes.string,
};

Footer.defaultProps = {
    text: '',
};

export default Footer