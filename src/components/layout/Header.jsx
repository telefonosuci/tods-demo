import React from 'react'
import CartMini from '../ui-elements/CartMini'
import NavBar from './NavBar'
import { Link } from "react-router-dom";
const Header = ({productId}) => {
    
    return (
        <div className='tods-c-header'>
            <div className='tods-c-header__top'>
                <h1 className='tods-c-header__title'>Bonfire</h1>
                <div className='tods-c-header__cartmini'>
                <CartMini />
                <Link
                    className='tods-c-header__checkout-btn'
                    to={{
                        pathname: "/checkout"
                    }}
                >Checkout</Link>
                </div>
            </div>
           
           <NavBar></NavBar>
        </div>
    )
}

export default Header