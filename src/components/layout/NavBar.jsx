import React from 'react'
import { Link } from "react-router-dom";

const NavBar = ({ productId }) => {

    return (
        <div className='tods-c-navbar'>
            <nav>
                <ul className='tods-c-navbar__ul'>
                    <li className='tods-c-navbar__li'><Link
                        className='tods-c-navbar__a'
                        to={{
                            pathname: "/"
                        }}
                    >Home</Link></li>
                    <li className='tods-c-navbar__li'><Link
                        className='tods-c-navbar__a'
                        to={{
                            pathname: "/"
                        }}
                    >Men</Link></li>
                    <li className='tods-c-navbar__li'><Link
                        className='tods-c-navbar__a'
                        to={{
                            pathname: "/"
                        }}
                    >Women</Link></li>
                    <li className='tods-c-navbar__li'><Link
                        className='tods-c-navbar__a'
                        to={{
                            pathname: "/"
                        }}
                    >Lookbook</Link></li>
                    <li className='tods-c-navbar__li'><Link
                        className='tods-c-navbar__a'
                        to={{
                            pathname: "/"
                        }}
                    >About</Link></li>
                    <li className='tods-c-navbar__li'><Link
                        className='tods-c-navbar__a'
                        to={{
                            pathname: "/"
                        }}
                    >Blog</Link></li>
                </ul>
            </nav>
        </div>
    )
}


export default NavBar