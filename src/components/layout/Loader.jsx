import React from "react";
import { useSelector } from 'react-redux'

const Loader = () => {
  const style = {
    overlay: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,0.4)', 
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    content: {
      width: '200px',
      height: '200px',
      border: "1px solid #ccc",
      background: "#fff",
      borderRadius: "4px",
      padding: "20px",
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  };

  const loading = useSelector(state => state.loading);

  return (
    <div>
        {loading && (
            <div style={style.overlay}>
                <div style={style.content}>
                    <div>Loading...</div>
                </div>
            </div>
        )}
    </div>
  );
}

export default Loader

