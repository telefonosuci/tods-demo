import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types';
import getProductService from '../../services/rest/products/getProduct'
import { createMarkup, searchVariantColorPriority, searchVariantSizePriority } from '../../utils/GlobalHelpers'
import Button from '../ui-elements/Button'
import SelectOptions from '../ui-elements/SelectOptions'
import QtySelect from '../ui-elements/QtySelect'
import { setLoading, addCartEntry, updateVariantSize, updateVariantColor, updateVariantOption } from '../../store/actions'
import _ from 'lodash'
import SlideShow from '../ui-elements/slide-show/SlideShow'

const ProductDetails = ({productId}) => {

    const [ productIdState, ] = useState(productId)
    const [ productState, setProductState ] = useState({})
    const [ qtyState, setQtyState ] = useState(0)
    const [ variantsBySize, setVariantsBySize ] = useState([])
    const [ variantsByColor, setVariantsByColor ] = useState([])

    const variantColor = useSelector(state => state.selectedColor);
    const variantSize = useSelector(state => state.selectedSize);
    const selectedOption = useSelector(state => state.selectedOption);

    const dispatch = useDispatch()

    const addToCartHandler = () => {
        if(!selectedOption){
            alert("Option not found")
        } else {
            if(selectedOption.stockLevel < qtyState) {
                alert("Not enough stock")
            } else {
                let entry = {
                    variantOption: selectedOption,
                    qty: qtyState,
                }
                dispatch(addCartEntry(entry))  
                alert('Product added to cart') 
            }
        }   
    }

    const changeVariantColorHandler = (value) => {
        let option 
        if(value) {
            option = searchVariantColorPriority(productState.variantOptions, value, variantSize)
            dispatch(updateVariantOption(option))
        }
    }

    const changeVariantSizeHandler = (value) => {
        let option 
        if(value) {
            option = searchVariantSizePriority(productState.variantOptions, value, variantColor)
            dispatch(updateVariantOption(option))
        }
    }

    const changeQtyHandler = (value) => {
        setQtyState(value)
    }

    const variantReducer = (list, field) => {
        const ret = _.groupBy(list, field)
        return ret
    }

    useEffect(() => {
        dispatch(setLoading(true))

        const getProduct = () => {
            getProductService({
                productId: productIdState,
                config: {},
                successCB: data => {
                    setProductState(data)
                    dispatch(setLoading(false))
                },
                failCB: err => {
                    console.error('Errore: ', err)
                    dispatch(setLoading(false))
                }
            })
        }
        getProduct()
    }, [dispatch, productIdState])

    useEffect(() => {
        setVariantsBySize(variantReducer(productState.variantOptions, 'size'))
        setVariantsByColor(variantReducer(productState.variantOptions, 'color'))
    }, [dispatch, productState])

    useEffect(() => {
        if(selectedOption){
            dispatch(updateVariantSize(selectedOption.size))
            dispatch(updateVariantColor(selectedOption.color))
        }
    }, [dispatch, selectedOption])


    return (
        <div className='tods-p-pdp'>
            <div className='tods-p-pdp__left-space'>
                <div>
                    <div className='tods-p-pdp__price-and-title tods-u-responsive__desktop-hide'>
                        <h1 className='tods-p-pdp__name'>Dettagli di {productState.name}</h1>
                        <div>
                            <h2 className='tods-p-pdp__price'>{productState.price?.formattedValue}</h2>
                        </div>
                        <div className='tods-p-pdp__props'>
                            <span className='tods-p-pdp__props-name'>Availability:</span><span className='tods-p-pdp__props-value'>{selectedOption?.stock?.stockLevelStatus}</span>
                        </div> 
                    </div>
                    <div className="tods-p-pdp__slider-container">
                        {productState.carouselImages && (
                            <SlideShow images={productState.carouselImages} />
                        )}
                    </div>
                </div>
            </div>
            <div className='tods-p-pdp__right-space'>
                <div>
                    <div className='tods-u-responsive__mobile-hide'>
                        <h1 className='tods-p-pdp__name'>Dettagli di {productState.name}</h1>
                        <div>
                            <h2 className='tods-p-pdp__price'>{productState.price?.formattedValue}</h2>
                        </div>   
                        <div className='tods-p-pdp__props'>
                            <span className='tods-p-pdp__props-name'>Availability:</span><span className='tods-p-pdp__props-value'>{selectedOption?.stock?.stockLevelStatus}</span>
                        </div>  
                    </div>
                    
                    <div className='tods-p-pdp__description-container'>
                        <p className='tods-p-pdp__description' dangerouslySetInnerHTML={createMarkup(productState.description)}></p>
                        <p className='tods-p-pdp__description'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>

                    
                    <div className='tods-p-pdp__pickers'>
                        {variantsByColor && 
                            <SelectOptions name='color' label='color' placeholder='Select color' options={variantsByColor} initialValue={selectedOption?.color} handler={changeVariantColorHandler} />
                        }
                        {variantsBySize && 
                            <SelectOptions name='size' label='size' placeholder='Select size' options={variantsBySize} initialValue={selectedOption?.size} handler={changeVariantSizeHandler} />
                        }

                        <QtySelect max={10} handler={changeQtyHandler} placeholder='Select qty' label='qty' />
                    </div>
                
                    
                    <div className='tods-p-pdp__actions'>
                        <Button text="Add to cart" onClick={addToCartHandler} disabled={!qtyState > 0 || !selectedOption}></Button>
                        <Button text="Add to wishlist" icon="heart"></Button>
                    </div>

                </div>
                
            </div>
        </div>
    )
}

ProductDetails.propTypes = {
    productId: PropTypes.string,
};

ProductDetails.defaultProps = {
    productId: '',
};

export default ProductDetails