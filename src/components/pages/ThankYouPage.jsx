import React from 'react'
import { useSelector } from 'react-redux'

const ThankYouPage = () => {
    const cartEntries = useSelector(state => state.cartEntries);

    const GetPrice = entry => {
        return <span>{entry.variantOption.priceData.value * entry.qty} {entry.variantOption.priceData.currencyIso}</span>
    }

    const GetTotal = entries => {
        let total = 0

        entries.forEach(entry => {
            total = total + entry.variantOption.priceData.value * entry.qty
        })
        
        return <span>Total: {total}</span>
    }

    return (
        <div className='tods-p-thank-you-page'>
            <div className='tods-p-thank-you-page__message-container'>
                <h1>Thank You!</h1>

                <h2>
                    Here's your order recap:
                </h2>
                <div>
                    <ul className='tods-p-thank-you-page__product-list'>
                        {cartEntries && cartEntries.map((entry, index) => {
                            
                        return <li key={index}><span>{entry.qty}</span> X <span>{entry.variantOption.color}</span>     <span>{entry.variantOption.size}</span> : {GetPrice(entry)}</li>
                        
                        })}
                    </ul>
                </div>

                <div className='tods-p-thank-you-page__total'>
                    {GetTotal(cartEntries)}
                </div>
            </div>
        </div>
    )
}

export default ThankYouPage