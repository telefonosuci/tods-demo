import React, { useEffect, useState } from 'react'
import InputText from '../ui-elements/InputText';
import { updateUserData } from '../../store/actions'
import { useDispatch, useSelector } from 'react-redux'
import RadioButton from '../ui-elements/RadioButton'
import { Link } from "react-router-dom";
import Button from '../ui-elements/Button'

const Checkout = () => {

    const [email, setEmail] = useState('')
    const [deliveryOptions] = useState([
        {
            placeholder: 'Modalità di spedizione Standard',
            value: 'standard',
            description: '3 - 5 giorni lavorativi'
        }, 
        {
            placeholder: 'Modalità di spedizione Express',
            value: 'express',
            description: '1 - 3 giorni lavorativi'
        }
    ])

    const userData = useSelector(state => state.userData);

    const dispatch = useDispatch()

    const emailInputHandler = (name, value) => {
        setEmail(value)
        dispatch(updateUserData({
            prop: name,
            value
        }))
    }

    const userDataInputHandler = (name, value) => {
        dispatch(updateUserData({
            prop: name,
            value
        }))
    }

    useEffect(() => {
        
    }, [])

    return (
        <div className='tods-p-checkout'>
            <h1 className='tods-p-checkout__title'>Checkout</h1>

            <div className='tods-p-checkout__form-container'>
                <div className='tods-p-checkout__form-group'>
                    <h2     className='tods-p-checkout__form-group-title'>Indirizzo email</h2>
                    <InputText name='email' type='text' placeholder='E-mail' initialValue={userData.email} value={email} handler={emailInputHandler} required={true}></InputText>

                </div>
                <div className='tods-p-checkout__form-group'>
                    <h2 className='tods-p-checkout__form-group-title'>Indirizzo di spedizione</h2>
                    <InputText name='nome' type='text' placeholder='Nome' initialValue={userData.nome} handler={userDataInputHandler} required={true}></InputText>
                    <InputText name='cognome' type='text' placeholder='Cognome' initialValue={userData.cognome} handler={userDataInputHandler} required={true}></InputText>
                    <InputText name='citta' type='text' placeholder='Città' initialValue={userData.citta} handler={userDataInputHandler} required={true}></InputText>
                    <InputText name='cap' type='text' placeholder='CAP' initialValue={userData.cap} handler={userDataInputHandler} required={true}></InputText>
                </div>
                <div className='tods-p-checkout__form-group'>
                    <h2 className='tods-p-checkout__form-group-title'>Metodo di spedizione</h2>
                    <RadioButton name='spedizione' options={deliveryOptions} handler={userDataInputHandler} initialValue={userData.spedizione}></RadioButton>
                </div>

                <Link
                    to={{
                        pathname: "/thankyou"
                    }}
                >
                    <Button text='Procedi al checkout' disabled={!userData.email || !userData.nome || !userData.cognome || !userData.citta || !userData.cap || !userData.spedizione }/>
                </Link>
            </div>
        </div>
    )
}

export default Checkout