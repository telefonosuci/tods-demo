const createProxyMiddleware = require('http-proxy-middleware');


module.exports = function (app) {
    app.use(
        '/rest/v2/tods-it/products/:id',
        createProxyMiddleware({
            target: 'https://stage.tods.com',
            changeOrigin: true,
        })
    );
};