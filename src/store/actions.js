import * as TYPES from './types';

export const setLoading = payload => ({
  type: TYPES.SET_LOADING,
  payload,
});

export const addCartEntry = payload => ({
    type: TYPES.ADD_CART_ENTRY,
    payload,
});

export const updateVariantSize = payload => ({
  type: TYPES.UPDATE_VARIANT_SIZE,
  payload,
});

export const updateVariantColor = payload => ({
  type: TYPES.UPDATE_VARIANT_COLOR,
  payload,
});

export const updateVariantOption = payload => ({
  type: TYPES.UPDATE_VARIANT_OPTION,
  payload,
});

export const updateUserData = payload => ({
  type: TYPES.UPDATE_USER_DATA,
  payload,
});