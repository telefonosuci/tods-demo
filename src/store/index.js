import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './reducers'

const composeEnhancers = composeWithDevTools({
  // options like actionSanitizer, stateSanitizer
});

function logger({ getState }) {
    return next => action => {
      console.log('REDUX - Will dispatch: ', action)
  
      // Call the next dispatch method in the middleware chain.
      const returnValue = next(action)
  
      console.log('REDUX - State after dispatch: ', getState())
  
      // This will likely be the action itself, unless
      // a middleware further in chain changed it.
      return returnValue
    }
  }

const store = createStore(reducers, 
  composeEnhancers(
    applyMiddleware(logger)
  )
)


export default store