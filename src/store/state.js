const state = {
    loading: false,
    cartEntries: [],
    selectedColor: '',
    selectedSize: '',
    selectedOption: {},
    userData: []
}

export default state