import * as TYPES from './types.js';
import initialState from './state'

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPES.SET_LOADING:
      return { ...state, loading: action.payload };
    case TYPES.ADD_CART_ENTRY:
      return {
        ...state,
        cartEntries: [...state.cartEntries, action.payload]
      };
    case TYPES.UPDATE_VARIANT_SIZE:
      return {
        ...state,
        selectedSize: action.payload
      };
    case TYPES.UPDATE_VARIANT_COLOR:
      return {
        ...state,
        selectedColor: action.payload
      };
    case TYPES.UPDATE_VARIANT_OPTION:
      return {
        ...state,
        selectedOption: action.payload
      };
    case TYPES.UPDATE_USER_DATA:
      return {
        ...state,
        userData: {
          ...state.userData,
          [action.payload.prop]: action.payload.value
        }
      };
      
    default:
      return state
  }
}

export default reducer