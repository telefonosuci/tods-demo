
// react related
export const createMarkup = str => ({ __html: str })

export const searchVariantColorPriority = (variantOptions, variantColor, variantSize) => {
    
    if( variantOptions ) {
        
        let options = variantOptions.filter(cv => {
            return cv.color === variantColor && cv.size === variantSize
        })

        if (!options.length)
            options = variantOptions.filter(cv => {
                return cv.color === variantColor
            })

        if (options[0])
            return options[0]
        else
            return null
    }else {
        return null
    }    
}

export const searchVariantSizePriority = (variantOptions, variantSize, variantColor) => {
    
    if( variantOptions ) {
        
        let options = variantOptions.filter(cv => {
            return cv.color === variantColor && cv.size === variantSize
        })

        if (!options.length)
            options = variantOptions.filter(cv => {
                return cv.size === variantSize
            })

        if (options[0])
            return options[0]
        else
            return null
    }else {
        return null
    }  
}