import './App.css';

import Header from './components/layout/Header'
import Footer from './components/layout/Footer'
import ProductDetails from './components/pages/ProductDetails'
import Checkout from './components/pages/Checkout'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { Provider } from "react-redux";
import store from './store'
import ThankYouPage from './components/pages/ThankYouPage';
import Loader from './components/layout/Loader';


function App() {
  return (
   
    <div className="App">
      <Provider store={store}>
        <Router>
          <Header />
          <Switch>
            <Route path="/" exact component={() => <ProductDetails productId='XXM0GW0CT55D9CS002' />} />
            <Route path="/checkout" exact component={() => <Checkout />} />
            <Route path="/thankyou" exact component={() => <ThankYouPage />} />
          </Switch>
          <Footer text="Made with Love and Cooffe 2015 Design Lazy. All right reserved."/>
          <Loader />
        </Router>
      </Provider>
    </div>
  );
}

export default App;
